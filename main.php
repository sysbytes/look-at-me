<div class="container-fluid">
    <section class="menu-bar">
        <div class="container">
            <ul class="hidden-xs">
                <li>
                    <a href="#dashboard" class="scroll-down">
                        <img src="img/ico-dashboard.png" class="img-responsive">
                        <span>Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="#datasources" class="scroll-down">
                        <img src="img/ico-datasource.png" class="img-responsive">
                        <span>Data source</span>
                    </a>
                </li>
                <li>
                    <a href="#reportes" class="scroll-down">
                        <img src="img/ico-reportes.png" class="img-responsive">
                        <span>Reports</span>
                    </a>
                </li>
                <li>
                    <a href="#manejo" class="scroll-down">
                        <img src="img/ico-manejos.png" class="img-responsive">
                        <span>Manejo de campañas</span>
                    </a>
                </li>
                <li>
                    <a href="#customizacion" class="scroll-down">
                        <img src="img/ico-customizacion.png" class="img-responsive">
                        <span>Customización</span>
                    </a>
                </li>
            </ul>
        </div>
    </section>

    <section class="main-banner"></section>

    <section class="about">
        <div class="container">
            <h1>i-Boards</h1>
            <p>Centralized enhanced reporting dash board, digital campaign reporting and ecosystem billing/payment tool. It tracks key performance indicators being executed through different channels for and by clients, agencies, publishers.</p>
        </div>

    </section>

    <section class="section">
        <div class="container">
            <div class="row section-content" id="dashboard">
                
                <div class="col-sm-6 col-sm-push-6">
                    <img src="img/img-dashboard.png" class="img-responsive" alt="Dashboard">
                </div>
                <div class="col-sm-6 col-sm-pull-6">
                    <h2>Dashboard</h2>
                    <p><strong>i-Boards</strong> presenta un tablero inteligente basado en medios digitales que funciona como una herramienta de visualización de datos que muestra el estado actual o pasado de las métricas y los indicadores clave de rendimiento (KPI) para una campaña de marketing digital. 
                    <strong>i-Boards</strong> consolida y organiza números, métricas y, en ocasiones, tablas de puntuación de rendimiento en una sola pantalla.<br>
                    A través de los Dashboards se pueden visualizar gráficas y datos, que pueden ser exportados por segmentos a CSV, Excel y PDF.
                </p>
                </div>

            </div>

            <div class="row section-content" id="datasources">
                <div class="col-sm-6">
                    <img src="img/img-datasource.jpg" class="img-responsive" alt="Data Source">
                </div>
                <div class="col-sm-6">
                    <h2>Data Sources</h2>
                    <p><strong>i-Boards</strong> permite la conexión de diferentes fuentes de datos que podrán ser visualizados y agrupados con las herramientas de Manejo de Campañas y Dashboards.<br>

					<strong>i-Boards</strong> maneja la conexión de Google Analytics, Google Search, Google Display, Facebook, Instagram y próximamente Twitter y LinkedIn. Fuentes adicionales de datos pueden ser ingresados a través de la customización de la plataforma.
					</p>
                </div>
            </div>

            <div class="row section-content" id="reportes">
                 <div class="col-sm-6 col-sm-push-6">
                    <img src="img/img-reportes.png" class="img-responsive" alt="Reportes">
                </div>
                <div class="col-sm-6 col-sm-pull-6">
                    <h2>Reportes</h2>
                    <p>El módulo de Reportes permite preparar informes para seguir el rendimiento de las campañas, analizando datos y realizando los cambios necesarios para optimizar los resultados hacia el cumplimiento de los objetivos.<br>

					Los reportes se pueden crear de forma general (Consolidados) o por cada fuente de datos manejada dentro del sistema, incluyendo la información financiera de cada campaña.
					</p>
                </div>
               
            </div>

            <div class="row section-content" id="manejo">
                <div class="col-sm-6">
                    <img src="img/img-manejo.png" class="img-responsive" alt="Manejo de Campañas">
                </div>
                <div class="col-sm-6">
                    <h2>Manejo de campañas</h2>
                    <p>El módulo de Manejo de Campañas permite agrupar las campañas en grupos de acuerdo con las necesidades de medición, esta opción facilita analizar los datos y agregarlos como parezca mejor en cada campaña.<br>

					Se debe crear un grupo de datos para acceder a la parte detallada del tablero y de cada servicio, lo que debe incluir las fuentes de datos y fechas de inicio y fin de la campaña a monitorear.
					</p>
                </div>
            </div>

            <div class="row section-content" id="customizacion">
                <div class="col-sm-6 col-sm-push-6">
                    <img src="img/img-customizacion.png" class="img-responsive" alt="Customización">
                </div>
                <div class="col-sm-6 col-sm-pull-6">
                    <h2>Customización</h2>
                    <p><strong>i-Boards</strong> es una plataforma abierta que permite la customización de sus diferentes módulos para conseguir ampliar la funcionalidad según las necesidades del cliente.<br>

					La customización puede incluir, nuevas fuentes de datos, importación y exportación de datos, accesos vía Single-sign On, nuevos reportes, nuevas variables de medición, entre otros.
					</p>
                </div>
            </div>
        </div>
    </section>

    <section class="contacto" id="contact">
        <h2 class="wow fadeInRight">CONTÁCTANOS</h2>
        <div class="container">
            <div class="row">
                <h3 class="wow zoomInLeft">¿Tiénes alguna pregunta?</h3>
                <div class="col-sm-6 col-sm-offset-3 col-xs-12 form__detail">
                    <form class="form-horizontal" id="formContact" role="form" action="" method="POST">
                        <div class="form-group ">
                            <div class="row">
                                <div class="col-sm-6 contact__nombre wow zoomInLeft">                                
                                    <input type="text" class="form-control" id="nombre" name="nombre" required placeholder="Nombre">
                                </div>
                                <div class="col-sm-6 contact__apellido  wow zoomInLeft">
                                    <input type="text" class="form-control" id="apellido" name="apellido" required placeholder="Apellido">
                                </div>
                            </div>
                        </div>
                         <div class="form-group ">
                            <div class="row">
                                <div class="col-sm-6 contact__nombre wow zoomInLeft">
                                    <input type="email" class="form-control" id="email" name="email" required placeholder="Email">
                                </div>
                                <div class="col-sm-6 contact__apellido  wow zoomInLeft">
                                    <input type="text" class="form-control" id="telefono" name="telefono" required placeholder="Teléfono">
                                </div>
                            </div>
                        </div>
                       
                        <div class="form-group  wow zoomInLeft">
                            <textarea class="form-control" id="mensaje" name="mensaje" rows="3" required placeholder="Mensaje"></textarea>
                        </div>
                        <div class="form-submit  wow zoomInLeft">
                            <button type="submit" class="btn btn-default btn-send">Enviar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <div class="gracias hidden">
        <div class="gracias__mensaje">
            <h2>¡Gracias por tu mensaje!</h2>
            <h4>Pronto nos pondremos en contacto.</h4>
        </div>
    </div>
</div>

<script type="text/javascript">

    
</script>