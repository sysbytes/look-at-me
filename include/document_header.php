<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta http-equiv="CACHE-CONTROL" content="NO-CACHE" />
    <meta name="description" content="" />
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Look-At-Me</title>
    <link rel="icon" type="image/png" href="img/favicon.png" />
    <meta property="og:url"         content="http://lookatme-ventures.com/" />
    <meta property="og:title"       content="Look-At-Me" />
    <meta property="og:description" content="" />
    <meta property="og:image"       content="" />
    <meta property="autor"          content="Wikot" />

    <!-- Bootstrap -->
    <!-- <link href="css/bootstrap.min.css" rel="stylesheet"> -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <link href="css/font-awesome.min.css" rel="stylesheet">

    <!-- Other CSS file -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css" rel="stylesheet">
    <link href='css/unite-gallery.css' rel='stylesheet' type='text/css' />

    <link href="css/styles.css" rel="stylesheet">
    <link href="css/<?php echo empty($modulo)?'home':$modulo;?>.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,500,700" rel="stylesheet">
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

    <!-- Other Library -->
    <script src="js/remodal.js" type="text/javascript" ></script>
    <script src="//cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js" type="text/javascript" ></script>
    <!-- UniteGallery -->
    <script src="js/unitegallery.min.js" type='text/javascript'></script>
    <script src='js/themes/tiles/ug-theme-tiles.js' type='text/javascript'></script>
    <!-- WOW -->
    <script src="js/wow.min.js" type="text/javascript"></script>
    <script>
        new WOW().init();
    </script>

    <!-- CUSTOM -->
    <script src="js/app.js" type="text/javascript" ></script>

</head>