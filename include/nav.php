<nav class="navbar navbar-default container-fluid  menu">
    <div class="container-fluid container-nav">
        <div class="container">
            <div class="navbar-header">
                <button type="button" id="bottom_menu" class="navbar-toggle collapsed pull-right" data-toggle="collapse" data-target="#navbar2">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="logo">
                    <a href="#home" class="scroll-down">
                        <img class="img-responsive" src="img/logo-look-at-me.png" alt="Look-At-Me" title="Look-At-Me" />
                    </a>
                </div>
                <!-- <ul class="nav navbar-nav navbar-right navbar-rrss visible-xs">
                   
                </ul> -->
                
            </div>
            <div id="navbar2" class="navbar-collapse collapse no-padding ">
                <ul class="nav navbar-nav navbar-right main-menu">
                    <li><a href="#" id="menu-1" class="active">i-Board</a></li>
                    <li><a href="#" id="menu-2" class="disabled">i-DMM</a></li>
                    <li><a href="#" id="menu-3" class="disabled">About</a></li>
                    <li><a href="#" id="menu-4" class="disabled">Blog</a></li>
                </ul>
                <!-- <ul class="nav navbar-nav navbar-right hidden-xs">

                </ul> -->
            </div>
        </div>
    </div>
</nav>